$(document).ready(function(){
    var html_data;

    function drawTable(model){
        $.get({
            url: '/load_vehicles/',
            async: false,
            data:{
                model_id: model
            },
            success: function(data){
                html_data = data.html;
            }});

        $('#table-container').html(html_data);
    }

    $('#submit-filter').on('click', function(){
        val = $('#select').val();
        console.log(val);
        drawTable(val);
    });


    drawTable('None');

});