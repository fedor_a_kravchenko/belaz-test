from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^load_vehicles/$', views.load_vehicles, name='load_vehicles'),
]