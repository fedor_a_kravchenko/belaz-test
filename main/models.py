from django.db import models

# Create your models here.

class VehicleModel(models.Model):
    name            =   models.CharField(max_length= 20, blank= False, null= False, verbose_name= 'Model Name')
    max_capacity    =   models.PositiveIntegerField(default = 100, blank= False, null= False,
                                                    verbose_name= 'Max Capacity (tonns)')

    class Meta:
        verbose_name            =   'Vehicle Model'
        verbose_name_plural     =   'Vehicle Models'

    def __str__(self):
        return self.name + ', max capacity: ' + str(self.max_capacity) + ' tonns'

    def save(self):
        super(VehicleModel, self).save()

    def delete(self):
        super(VehicleModel, self).delete()


class Vehicle(models.Model):
    name            =   models.CharField(max_length= 20, blank= False, null= False, verbose_name= 'Vehicle Name')
    model           =   models.ForeignKey(VehicleModel, blank= False, null= False)
    curr_capacity   =   models.PositiveIntegerField(default= 0, blank= False, null= False, verbose_name= 'Current Capacity')

    class Meta:
        verbose_name            =   'Vehicle'
        verbose_name_plural     =   'Vehicles'

    def __str__(self):
        return self.name + ' (' + self.model.name + ')'

    def save(self):
        super(Vehicle, self).save()

    def delete(self):
        super(Vehicle, self).delete()