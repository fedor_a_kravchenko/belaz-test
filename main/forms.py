from django import forms
from .models import VehicleModel, Vehicle

class VehicleListForm(forms.ModelForm):
    model_name      =   forms.CharField()
    max_capacity    =   forms.IntegerField()
    overload        =   forms.DecimalField()

    class Meta:
        model       =   Vehicle
        fields      =   ['name', 'curr_capacity' ]

    def __init__(self, *args, **kwargs):
        super(VehicleListForm, self).__init__(*args, **kwargs)
        self.vehicle_model      =   self.instance.model
        self.order_fields(['name', 'model_name', 'max_capacity',  'curr_capacity', 'overload', ])
        if 'name' in self.fields:
            self.fields['name'].label           =   'Бортовой Номер'

        if 'model_name' in self.fields:
            self.fields['model_name'].initial   =   self.vehicle_model.name
            self.fields['model_name'].label     =   'Модель'

        if 'max_capacity' in self.fields:
            self.fields['max_capacity'].initial =   self.vehicle_model.max_capacity
            self.fields['max_capacity'].label   =   'Макс. Грузоподъемность'

        if 'curr_capacity' in self.fields:
            self.fields['curr_capacity'].label  =   'Текущий Вес'

        if 'overload' in self.fields:
            self.fields['overload'].initial     =   self.instance.overload
            self.fields['overload'].label       =   'Перегруз, %'
