from django.contrib import admin
from .models import VehicleModel, Vehicle

# Register your models here.

class VehicleModelAdmin(admin.ModelAdmin):
    list_display            =   ['id', 'name', 'max_capacity', ]
    list_display_links      =   ['id', 'name', 'max_capacity', ]
    search_fields           =   ['name', 'max_capacity', ]
    ordering                =   ['id', ]

admin.site.register(VehicleModel, VehicleModelAdmin)


class VehicleAdmin(admin.ModelAdmin):
    list_display            =   ['id', 'model_name', 'max_capacity', 'curr_capacity', ]
    list_display_links      =   ['id', 'model_name', 'max_capacity', 'curr_capacity', ]
    search_fields           =   ['model', 'model_name', 'max_capacity', 'curr_capacity', ]
    ordering                =   ['id', ]

    def model_name(self, obj):
        if obj.model == None:
            return 'N/A'
        else:
            return obj.model.name

    def max_capacity(self, obj):
        if obj.model == None:
            return 'N/A'
        else:
            return obj.model.max_capacity

admin.site.register(Vehicle, VehicleAdmin)