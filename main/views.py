from django.shortcuts import render
from django.forms import modelformset_factory
from .models import VehicleModel, Vehicle
from .forms import VehicleListForm
from django.db.models import F, Value, ExpressionWrapper, DecimalField
from django.db.models.functions import Greatest, Cast

from django.http import HttpResponse
import json

from django.template.loader import render_to_string

# Create your views here.

def index(request):
    if request.method == 'GET':
        vehicle_models  =   VehicleModel.objects.all()
        models_list     =   [{'value': i.id, 'displayed_value': i.name} for i in vehicle_models]
        models_list.insert(0, {'value': None, 'displayed_value':'-- Все --'})

        return render(request, 'main/index.html', {'models': models_list})

def load_vehicles(request):
    if request.method == 'GET':
        ObjFormSet = modelformset_factory(Vehicle, extra=0, form=VehicleListForm)
        model_id        =   request.GET['model_id']
        if model_id != 'None':
            QS          =   Vehicle.objects.filter(model__id=int(model_id)).annotate(overload= ExpressionWrapper(Cast(
                Greatest((F('curr_capacity') - F('model__max_capacity')) * Value(100.0) / F('model__max_capacity'), 0),
                DecimalField(max_digits=5, decimal_places=2)), output_field= DecimalField(max_digits= 5, decimal_places= 2)))

        else:
            QS          =   Vehicle.objects.all().annotate(overload= ExpressionWrapper(Cast(
                Greatest((F('curr_capacity') - F('model__max_capacity')) * Value(100.0) / F('model__max_capacity'), 0),
                DecimalField(max_digits=5, decimal_places=2)), output_field= DecimalField(max_digits= 5, decimal_places= 2)))
        formset = ObjFormSet(queryset= QS)
        response_data   =   {'html': render_to_string('main/table.html', {'formset': formset})}
        return HttpResponse(json.dumps(response_data), content_type="application/json")